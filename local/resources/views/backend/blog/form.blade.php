
      @include('include.admin.backend_header')
      <!-- Left side column. contains the logo and sidebar -->
     
          @include('include.admin.backend_sidebar')
      <!-- Content Wrapper. Contains page content -->
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datetimepicker/DateTimePicker.css')}}" /> -->

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Blog
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
           
            <li class="active">Blog</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Form</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                  <?php 

                  $success = session('data'); 
                  if(!empty($success)) : 
                    if($success['success'] == true)
                  ?>
                  <div class="box-footer results">
                    <div class="alert alert-success">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> Blog has been inserted.
                    </div>
                  </div>
                <?php endif; ?>
                  <form  id="" action="{{route('postblog')}}" method="post"  accept-charset="UTF-8" enctype="multipart/form-data">   
            
                  <div class="box-body">
                    <div class="form-group">
                      <label>Title</label>
                      <input type="text" class="form-control" name="title" placeholder="Title"   required>
                    </div>
                      
                      <div class="form-group">
                        <label >Description</label>
                        <textarea name="description" id="editor1" rows="10" cols="80" required>
                          
                        </textarea>
                      </div>

                      <div class="form-group">
                           <div class="row">
                                           <div class="col-lg-12">
                                             <!-- <div class="img_uploadadminapproval">Note: Please select an image 212px/73px</div> -->
                                          <div class="col-sm-6">
                       <label>Image </label>
                          <input type="file" name="blog_image" class="form-control" id="catagry_logo" required>
                      
                      </div>                  
                      
                                        </div>
                                        </div>
                      </div>
                     <input type="hidden" name="_token" value="{{ csrf_token()}}">
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" name="dataImageForm" class="btn btn-primary">Submit</button>
                  </div>
                  
                </form>
                 
                    <div id="dtBox"></div>
                   <div class="ag_msg"></div>
                  <div id="output1"></div> 
           
 
              </div><!-- /.box -->

            </div><!--/.col (left) -->

          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      @include('include.admin.footer')

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="{{asset('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('assets/admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('assets/admin/plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('assets/admin/dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('assets/admin/dist/js/demo.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/js/datetimepicker/DateTimePicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/datetimepicker/DateTimePicker-i18n.js')}}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.7.0/ckeditor.js"></script>


 <script src="http://malsup.github.com/jquery.form.js"></script>
    <script type="text/javascript">
  
  

$('#myForm').ajaxForm(function(options) { 
              if(options == 'success'){
                $("#myForm")[0].reset();
                CKEDITOR.instances.editor1.setData('');
                $('.results').css('display','block');
              }
              
           });

</script>
<script type="text/javascript">
$(document).ready(function()
 {
       
     $("#dtBox").DateTimePicker();
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace( 'editor1' );
  
 });
   </script>

  </body>
</html>
